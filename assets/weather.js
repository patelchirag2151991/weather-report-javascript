(function geolocation (){
	try
	{
		if (navigator.geolocation){
			navigator.geolocation.getCurrentPosition(getcoordinates);
		}
		else {
			document.getElementById("weather").html("Geolocation is not supported by this browser.");
		}
	}
	catch(e)
	{
		console.log(e.message);
	}
})();
function getcoordinates(position) {
	try
	{
		var lat = position.coords.latitude;;
		var long = position.coords.longitude;
		var units=localStorage.getItem("Units");
		var CurrentWeatherURL = "http://api.openweathermap.org/data/2.5/weather?lat="+lat+"&lon="+long+"&units="+units+"&appid=2de143494c0b295cca9337e1e96b00e0";
		var DailyForecastURL = "http://api.openweathermap.org/data/2.5/forecast/daily?lat="+lat+"&lon="+long+"&units="+units+"&cnt=1&appid=2de143494c0b295cca9337e1e96b00e0";
		if (units == "imperial") {
			getWeather(CurrentWeatherURL)
		}
		else {
			getWeather(CurrentWeatherURL)
		}
	}
	catch(e)
	{
		console.log(e.message);
	}
	
}
function getWeather(url)
{
	try
	{
		xmlHttp = new XMLHttpRequest(); 
		xmlHttp.onreadystatechange = getWeatherCallBack;
		xmlHttp.open( "GET", url, true );
		xmlHttp.send();
	}
	catch(e)
	{
		console.log(e.message);
	}
}

function getWeatherCallBack() 
{
	try
	{
		if ( xmlHttp.readyState == 4 && xmlHttp.status == 200 ) 
		{
			if ( xmlHttp.responseText == "Not found" ) 
			{
			   
			}
			else
			{
				var info = eval ( "(" + xmlHttp.responseText + ")" );
				localStorage.WeatherCache = JSON.stringify(xmlHttp.responseText);
				displayData();
			}                    
		}
		else
		{
			document.getElementById("weather").innetHTML = "<p></p>";
		}
	}
	catch(e)
	{
		console.log(e.message);
	}
}
function displayData() {
    try 
	{
        // If the timestamp is newer  than 30 minutes, parse data from cache
        var finalValue = "";
		var data = JSON.parse(localStorage.WeatherCache);
		data = JSON.parse(data);
		document.body.style.background = "url('assets/backgrounds/" +data.weather[0].icon+ ".jpg') no-repeat fixed 50% 50%";
		document.body.style.backgroundSize = "cover";
		
		finalValue = finalValue + '<h2>' + data.name + ', ' + data.sys.country + '</h2>';
		finalValue = finalValue + '<img class="icon" src="assets/icons/'+data.weather[0].icon+'.png"><span id="temp">'+ data.main.temp + ' </span>';
		finalValue = finalValue + '<p id="description"> Cloudiness	: '+ data.weather[0].description + '</p>';
		finalValue = finalValue + '<p id="pressure"> Pressure	: '+ data.main.pressure + ' hpa</p>';
		finalValue = finalValue + '<p><span id="humidity"> Humidity : '+ data.main.humidity + '%</span></p>';
		finalValue = finalValue + '<p><span id="geoCoords"> Geo coords : ['+ data.coord.lon + ', ' + data.coord.lat +']</span></p>';
		
		document.getElementById("weather").innerHTML = finalValue;
    }
    catch(e)
	{
        console.log(e.messaage);
    }
}
function SetCelsius()
{
	try
	{
		localStorage.Units = "metric";
		location.reload();
	}
	catch(e)
	{
		console.log(e.message);
	}
}
function SetFahrenheit() 
{
	try
	{
		localStorage.Units = "imperial";
		location.reload();
	}
	catch(e)
	{
		console.log(e.message);
	}
}